"""
Cubito
"""
from itertools import count
from time import sleep
from cubito import cubetto_forward, cubetto_left, cubetto_right, cubetto_backward

import arcade
# import cubito

# Screen title and size
SCREEN_TITLE = "Cubito"
SCREEN_WIDTH = 500
SCREEN_HEIGHT = 430

# Margin between mat & screen side
VERTICAL_MARGIN_PERCENT = 0.10
HORIZONTAL_MARGIN_PERCENT = 0.10

# Token size
TOKEN_SCALE = 0.5
TOKEN_HEIGHT = 75 * TOKEN_SCALE
TOKEN_WIDTH = 75 * TOKEN_SCALE

# Space between tokens
X_SPACING_TOKEN = TOKEN_WIDTH + TOKEN_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_TOKEN = TOKEN_HEIGHT + TOKEN_HEIGHT * VERTICAL_MARGIN_PERCENT

# List of token types
TOKEN_TYPES = ["FORDWARD", "RIGHT", "LEFT", "PAUSE", "FUNCTION"]

# Mat size
MAT_PERCENT_OVERSIZE = 1.25
MAT_HEIGHT = int(TOKEN_HEIGHT * MAT_PERCENT_OVERSIZE)
MAT_WIDTH = int(TOKEN_WIDTH * MAT_PERCENT_OVERSIZE)

# Number of column & row mat
MAT_COLUMN = 4
MAT_ROW = 4
MAT_FUNCTION_ROW = 2

# Space between mats
X_SPACING_MAT = MAT_WIDTH + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT
Y_SPACING_MAT = MAT_HEIGHT + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Top for mats
TOP_Y = SCREEN_HEIGHT - MAT_HEIGHT - MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Bottom for tokens
BOTTOM_Y = MAT_HEIGHT / 2 + MAT_HEIGHT * VERTICAL_MARGIN_PERCENT

# Start from left side
START_X = MAT_WIDTH / 2 + MAT_WIDTH * HORIZONTAL_MARGIN_PERCENT

# Mat pos
X_MAT_POS = START_X + SCREEN_WIDTH / 2
Y_MAT_POS = TOP_Y

class Token(arcade.Sprite):
    """Token sprite"""

    sprite = {
        "FORDWARD": "up",
        "RIGHT": "r",
        "LEFT": "l",
        "FUNCTION": "star_round",
        "PAUSE": "cancel"
    }

    def __init__(self, token_type, scale=1):
        """Token constructor"""
        # Type of token
        self.type = token_type
        # Get sprite for type of token
        self.image_file_name = (
            ":resources:onscreen_controls/shaded_dark/%s.png"
            % self.sprite.get(token_type, "unchecked")
        )

        # Init parent class
        super().__init__(self.image_file_name, scale, hit_box_algorithm="None")


class Mat(arcade.SpriteSolidColor):
    """Mat sprite"""

    # Count instance mat classes
    ids = count(0)

    def __init__(self, wigth, height, color=arcade.color.BLEU_DE_FRANCE):
        # Get id from number of instance classes
        self.id = next(self.ids)

        # Init parent class
        super().__init__(wigth, height, color)

class Mat_function(arcade.SpriteSolidColor):
    """Mat sprite"""

    # Count instance mat classes
    ids = count(0)

    def __init__(self, wigth, height, color=arcade.color.BLEU_DE_FRANCE):
        # Get id from number of instance classes
        self.id = next(self.ids)

        # Init parent class
        super().__init__(wigth, height, color)

class Cubito(arcade.Window):
    """Main application class"""

    def __init__(self):
        # Init parent class
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
        # Set background color
        arcade.set_background_color(arcade.color.AIR_SUPERIORITY_BLUE)

        # List of tokens
        self.token_list = None

        # Hold token
        self.held_token = None

        # Origin pos for hold token
        self.held_token_original_position = None

        # List of mats
        self.mat_list = None

        # List of mats function
        self.mat_function_list = None

    def setup(self):
        """Set up the game"""
        # List of cards we are dragging with the mouse
        self.held_token = None

        self.held_token_original_position = None

        self.mat_list = arcade.SpriteList()

        self.mat_function_list = arcade.SpriteList()

        for y in range(MAT_ROW):
            # Create the top "play" piles
            for x in range(MAT_COLUMN):
                mat = Mat(MAT_WIDTH, MAT_HEIGHT)
                mat.position = (
                    X_MAT_POS + X_SPACING_MAT * x,
                    Y_MAT_POS - Y_SPACING_MAT * y - 60
                )
                self.mat_list.append(mat)
        
        self.mat_function_list = arcade.SpriteList()

        for x in range(MAT_COLUMN):
            mat = Mat_function(MAT_WIDTH, MAT_HEIGHT)
            mat.position = (
                X_MAT_POS + X_SPACING_MAT * x,
                Y_MAT_POS - Y_SPACING_MAT * y - 150
            )
            self.mat_function_list.append(mat)

        # Sprite list with all the cards, no matter what pile they are in.
        self.token_list = arcade.SpriteList()

        # Create every token
        for j, t in enumerate(TOKEN_TYPES):
            for i in range(10):
                token = Token(t, TOKEN_SCALE)
                token.position = (
                    START_X + X_SPACING_TOKEN * j,
                    BOTTOM_Y + X_SPACING_TOKEN * i,
                )
                self.token_list.append(token)
                i += 50

    def on_draw(self):
        """Render the screen"""
        # Clear the screen
        arcade.start_render()

        # Draw the mats
        self.mat_list.draw()

        # Draw the mats function
        self.mat_function_list.draw()

        # Draw the tokens
        self.token_list.draw()

    def on_mouse_press(self, x, y, button, key_modifiers):
        """Called when the user presses a mouse button"""
        # Get list of token when click
        token = arcade.get_sprites_at_point((x, y), self.token_list)

        # Check if grab a token
        if len(token) > 0:
            # Hold the token
            self.held_token = token[-1]
            # Set original pos for this token
            self.held_token_original_position = self.held_token.position

    def on_mouse_release(self, x, y, button, modifiers):
        """Called when the user presses a mouse button"""

        # If we don't have any tokens, who cares
        if self.held_token is None:
            return

        # Find the closest pile, in case we are in contact with more than one
        mat, distance = arcade.get_closest_sprite(self.held_token, self.mat_list)
        mat_function, distance = arcade.get_closest_sprite(self.held_token, self.mat_function_list)
        reset_position = True

        # See if we are in contact with the closest mat
        if arcade.check_for_collision(self.held_token, mat):

            self.held_token.position = mat.center_x, mat.center_y

            # Success, don't reset position of cards
            reset_position = False
        
        if arcade.check_for_collision(self.held_token, mat_function):
            token, distance = arcade.get_closest_sprite(self.held_token, self.token_list)
            if token.type != "FUNCTION":
                self.held_token.position = mat_function.center_x, mat_function.center_y
            else:
                self.held_token.position = self.held_token_original_position

            # Success, don't reset position of cards
            reset_position = False

        # Release on top play pile? And only one card held?
        if reset_position:
            # Where-ever we were dropped, it wasn't valid. Reset the each card's position
            # to its original spot.
            self.held_token.position = self.held_token_original_position

        # We are no longer holding tokens
        self.held_token = None

    def on_mouse_motion(self, x, y, dx, dy):
        """Called when the user moves the mouse"""
        # If no token hold, pass
        if self.held_token is None:
            return

        self.held_token.center_x += dx
        self.held_token.center_y += dy

    def on_key_press(self, symbol, modifiers):
        """Called when the user presses key"""

        if symbol == arcade.key.L:

            token_type_list = []
            
            for i in self.mat_list:
                token, distance = arcade.get_closest_sprite(i, self.token_list)
                if arcade.check_for_collision(i, token):
                        token_type_list.append(token.type)

            for t in token_type_list:
                if t == "FORDWARD":
                    cubetto_forward()
                elif t == "LEFT":
                    cubetto_left()
                elif t == "RIGHT":
                    cubetto_right() 
                elif t == "FUNCTION":
                    for i in self.mat_function_list:
                        token, distance = arcade.get_closest_sprite(i, self.token_list)
                        if arcade.check_for_collision(i, token):
                            token_type_list.append(token.type)
                elif t == "PAUSE":
                    sleep(1)


        # Press « R » key
        if symbol == arcade.key.R:
            print("Restart !")
            # Relunch setup methode
            self.setup()

        # Press « Q » key
        if symbol == arcade.key.Q:
            print("Good bye !")
            # Exit
            arcade.exit()

    def cubito(self, function=False):
        """Move cubito !"""
        

def main():
    """Main method"""
    window = Cubito()
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()