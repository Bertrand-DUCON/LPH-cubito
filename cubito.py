from turtle import *
import serial

angle = 0

def cubetto_init():
    
    bgpic("fond.png")
    register_shape("Cubetto_forme_haut.gif")
    register_shape("Cubetto_forme_bas.gif")
    register_shape("Cubetto_forme_gauche.gif")
    register_shape("Cubetto_forme_droite.gif")
    shape("Cubetto_forme_droite.gif")
    shapesize(3,3,6)
    penup()
    goto(-210,194)

x_c = 0
y_c = 0
sh = "d"
def cubetto_forme_init():
    global angle, sh
    sh = "d"
    if angle == 360 or angle == -360:
        angle=0
        sh = "d"
    if angle == 0:
        shape("Cubetto_forme_droite.gif")
        sh = "d"
    if angle == 90 or angle == -270:
        shape("Cubetto_forme_haut.gif")
        sh = "h"
    if angle == 180 or angle == -180:
        shape("Cubetto_forme_gauche.gif")
        sh = "g"
    if angle == 270 or angle == -90:
        shape("Cubetto_forme_bas.gif")
        sh = "b"

def cubetto_forward():
    global sh, x_c, y_c
    forward(84)
    print(sh, x_c)
    if sh == "d" and x_c<=336 and x_c>=0:
        x_c+=84
    if sh == "h" and y_c<=(4*84) and y_c>=0:
        y_c-=84

def cubetto_left():
    global angle
    left(90)
    angle += 90
    cubetto_forme_init()

def cubetto_right():
    global angle
    right(90)
    angle -= 90
    cubetto_forme_init()

def cubetto_backward():
    right(180)
    forward(84)
    right(180)


cubetto_init()


